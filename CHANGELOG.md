# Changelog

All notable changes to this project will be documented in this file.

## [1.1.5] - 2025-02-25

### 🐛 Bug Fixes

- *(deploy)* Update labels to use recommended format
- *(go.mod)* Update Go version and toolchain version
- *(deps)* Update kubernetes packages to v0.32.2
- *(docker)* Update golang base image to use amd64 tag

## [1.1.4] - 2025-01-17

### 🐛 Bug Fixes

- *(deps)* Update kubernetes packages to v0.32.1

## [1.1.3] - 2024-12-28

### 🐛 Bug Fixes

- *(deps)* Update kubernetes packages to v0.31.4
- *(deps)* Update kubernetes packages to v0.32.0

### ⚙️ Miscellaneous Tasks

- Remove unnecessary variables and services from CI config

## [1.1.2] - 2024-11-21

### 🐛 Bug Fixes

- *(deps)* Update kubernetes packages to v0.31.3

## [1.1.1] - 2024-10-24

### 🐛 Bug Fixes

- *(deps)* Update kubernetes packages to v0.31.2

## [1.1.0] - 2024-10-06

### 🐛 Bug Fixes

- *(deps)* Update kubernetes packages to v0.29.3
- *(deps)* Update module k8s.io/client-go to v0.29.4
- *(deps)* Update kubernetes packages to v0.30.0
- *(deps)* Update kubernetes packages to v0.30.1
- *(deps)* Update kubernetes packages to v0.30.2
- *(deps)* Update kubernetes packages to v0.30.3
- *(deps)* Update kubernetes packages to v0.31.0
- *(deps)* Update kubernetes packages to v0.31.1

### ⚙️ Miscellaneous Tasks

- Add dependabot-config
- Use Docker DinD version from variable
- Move build inside Dockerfile
- Switch to manual rebases for Dependabot
- Remove Dependabot config
- Add release flow
- Add git-cliff configuration

<!-- generated by git-cliff -->
