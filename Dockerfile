FROM amd64/golang:1.24.1@sha256:f4707a73c390cc4ee035f7527f25907c3e06745ab946628f72cb459b2e70d401 as deps
WORKDIR /build
ADD go.* /build
RUN go mod download

FROM deps as build
ENV CGO_ENABLED=0
ADD . /build
RUN if [ $(go mod tidy -v 2>&1 | grep -c unused) != 0 ]; then echo "Unused modules, please run 'go mod tidy'"; exit 1; fi
RUN go fmt ./...
RUN go vet ./...
RUN CGO_ENABLED=1 go test -mod=readonly -race -coverprofile=coverage.txt.tmp -covermode=atomic -coverpkg=$(go list ./... | tr '\n' , | sed 's/,$//') ./...
RUN ["/bin/bash", "-c", "cat coverage.txt.tmp | grep -v -f <(find . -type f | xargs grep -l 'Code generated') > coverage.txt"]
RUN go tool cover -html=coverage.txt -o coverage.html
RUN go tool cover -func=coverage.txt
RUN rm coverage.txt.tmp
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -mod=readonly -o release/default-request-adder -ldflags '-w -s'

FROM scratch as export
COPY --from=build /build/coverage.txt /

FROM scratch
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /build/release/default-request-adder /
CMD ["/default-request-adder"]
